<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Get available products
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableProducts(Request $request)
    {
        $data = Product::query()
            ->with('avialable',function ($query){
                $query->selectRaw("product_id,
                                        storage_id,
                                        SUM(
                                            CASE WHEN order_type IN('sell', 'buy_refund') THEN -1 * product_amount ELSE product_amount
                                        END
                                    ) AS product_amount");
                $query->groupBy('product_id','storage_id');
            })
            ->whereRaw("exists (SELECT
                                        product_id,
                                        storage_id,
                                        SUM(
                                            CASE WHEN order_type IN('sell', 'buy_refund') THEN -1 * product_amount ELSE product_amount
                                        END
                                    ) AS product_amount
                                    FROM
                                        order_items
                                    WHERE products.id = order_items.product_id
                                    GROUP BY
                                        product_id,
                                        storage_id)");

        if ($request->has('category_id')){
            $category_id = $request->category_id;
            $data = $data->whereHas('category',function ($query) use ($category_id){
                $query->where('id','=',$category_id);
            })->orWhereHas('category.parent',function ($query) use ($category_id){
                $query->where('id','=',$category_id);
            });
        }

        $data = $data->paginate(10);

        return $this->ResponseSuccess($data);
    }
}
