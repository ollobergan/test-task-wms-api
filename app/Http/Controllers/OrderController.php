<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductBuyRequest;
use App\Http\Requests\ProductSellRequest;
use App\Http\Requests\RefundBuyedProductRequest;
use App\Models\Order;
use App\Services\OrderProductService;
use App\Services\OrderRefundService;
use App\Services\OrderService;
use App\Services\ProductService;

class OrderController extends Controller
{

    /**
     * Method for buy product from providers
     *
     * @param ProductBuyRequest $productBuyRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \ErrorException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function buy_products(ProductBuyRequest $productBuyRequest)
    {
        //Validate products
        $productService = new ProductService();
        foreach($productBuyRequest->products as $product) {
            $productService->checkProviderProduct($product['id'],$productBuyRequest->account_id);
            $productService->checkProductMeasure($product['id'],$product['measure_id']);
        }
        $order = $productBuyRequest->all();
        $order['order_type'] = 'buy';
        return $this->ResponseSuccess((new OrderService())->createOrder($order));
    }

    /**
     * Mathod for refund buyed products
     *
     * @param RefundBuyedProductRequest $refundBuyedProductRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \ErrorException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function refund_buyed_products(RefundBuyedProductRequest $refundBuyedProductRequest)
    {
        //Validate
        $order = Order::find($refundBuyedProductRequest->order_id);
        foreach($refundBuyedProductRequest->products as $product) {
            (new ProductService())->checkProductMeasure($product['id'],$product['measure_id']);
            (new OrderProductService($refundBuyedProductRequest->order_id))->checkProductForBuy($product, $order->storage_id);
        }

        // Create refund
        (new OrderRefundService($refundBuyedProductRequest->order_id))->refundBuyedProduct($refundBuyedProductRequest->products);

        return $this->ResponseSuccess();
    }

    /**
     * Method for sell products
     *
     * @param ProductSellRequest $productSellRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \ErrorException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sell_products(ProductSellRequest $productSellRequest)
    {
        foreach($productSellRequest->products as $product) {
            (new ProductService())->checkProductMeasure($product['id'],$product['measure_id']);
            (new OrderProductService())->checkProductForBuy($product,$productSellRequest->storage_id);
        }
        $order = $productSellRequest->all();
        $order['order_type'] = 'sell';
        return $this->ResponseSuccess((new OrderService())->createOrder($order));
    }
}
