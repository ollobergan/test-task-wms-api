<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * Helper for success responses
     *
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function ResponseSuccess($data = null)
    {
        return response()->json([
            "success"       => true,
            "message"       => trans('api.success'),
            "message_code"  => "success",
            "data"          => $data
        ]);
    }

    /**
     * Helper for success responses with data
     *
     * @param $message_code
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function ResponseSuccessMessage($message_code,$data = null)
    {
        return response()->json([
            "success"       => true,
            "message"       => trans('api.'.$message_code),
            "message_code"  => $message_code,
            "data"          => $data
        ]);
    }

    /**
     * Helper for error responses
     *
     * @param $message_code
     * @param $http_code
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function ResponseError($message_code, $http_code, $data = null)
    {
        return response()->json([
            "success"       => false,
            "message"       => trans('api.'.$message_code),
            "message_code"  => $message_code,
            "data"          => $data
        ], $http_code);
    }
}
