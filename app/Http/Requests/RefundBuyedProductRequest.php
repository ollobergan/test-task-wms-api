<?php

namespace App\Http\Requests;

use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RefundBuyedProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "order_id"=>[
                "required",
                Rule::exists('orders','id')->where(function (Builder $query) {
                    return $query->where('order_type', '=','buy');
                })
            ],
            'products'              => 'required|array',
            'products.*.id'         => 'required|exists:products',
            'products.*.amount'     => 'required|numeric|min:0',
            'products.*.measure_id' => 'required|numeric',
        ];
    }
}
