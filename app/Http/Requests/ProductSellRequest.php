<?php

namespace App\Http\Requests;

use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductSellRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'account_id'           => [
                'required',
                Rule::exists('accounts','id')->where(function (Builder $query) {
                    return $query->where('account_type', '=','client');
                })
            ],
            'storage_id'           => [
                'required',
                Rule::exists('storages','id')
            ],
            'more'                  => 'max:255',
            'products'              => 'required|array',
            'products.*.id'         => 'required|exists:products',
            'products.*.amount'     => 'required|numeric|min:0',
            'products.*.price'      => 'required|numeric|min:0',
            'products.*.measure_id' => 'required|numeric',
        ];
    }
}
