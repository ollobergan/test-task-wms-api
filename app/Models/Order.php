<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "orders";
    protected $fillable = [
        "account_id",
        "storage_id",
        "order_type",
        "order_date",
        "order_total_sum",
        "order_total_sum_refund",
        "order_total_sum_success",
        "more",
        "user_id",
    ];

    protected $casts = [
        "account_id"                => "integer",
        "storage_id"                => "integer",
        "order_type"                => "string",
        "order_date"                => "datetime:Y-m-d H:m:s",
        "order_total_sum"           => "float",
        "order_total_sum_refund"    => "float",
        "order_total_sum_success"   => "float",
        "more"                      => "string",
        "user_id"                   => "integer",
        "created_at"                => "datetime:Y-m-d H:m:s",
        "updated_at"                => "datetime:Y-m-d H:m:s"
    ];
}
