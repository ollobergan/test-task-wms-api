<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $table = "accounts";

    protected $fillable = [
        "account_type",
        "name",
        "phone",
    ];

    protected $casts = [
        "account_type"  => "string",
        "name"          => "string",
        "phone"         => "string",
        "created_at"    => "datetime:Y-m-d H:m:s",
        "updated_at"    => "datetime:Y-m-d H:m:s"
    ];

}
