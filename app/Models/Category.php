<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = "categories";
    protected $fillable = [
        "parent_id",
        "name",
    ];
    protected $casts = [
        "parent_id"     => "integer",
        "name"          => "string",
        "created_at"    => "datetime:Y-m-d H:m:s",
        "updated_at"    => "datetime:Y-m-d H:m:s"
    ];

    public function parent(){
        return $this->belongsTo(self::class,'parent_id');
    }
}
