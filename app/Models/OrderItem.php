<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;
    protected $table = "order_items";
    protected $fillable = [
        "account_id",
        "storage_id",
        "order_type",
        "product_id",
        "product_amount",
        "product_measure_id",
        "amount",
        "measure_id",
        "product_price",
        "more",
        "user_id",
    ];

    protected $casts = [
        "account_id"            => "integer",
        "storage_id"            => "integer",
        "order_type"            => "string",
        "product_id"            => "integer",
        "product_amount"        => "float",
        "measure_amount"        => "float",
        "product_measure_id"    => "integer",
        "product_price"         => "float",
        "more"                  => "string",
        "user_id"               => "integer",
        "created_at"            => "datetime:Y-m-d H:m:s",
        "updated_at"            => "datetime:Y-m-d H:m:s"

    ];
}
