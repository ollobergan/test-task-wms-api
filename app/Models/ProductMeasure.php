<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMeasure extends Model
{
    use HasFactory;
    protected $table = "product_measures";
    protected $fillable = [
        "product_id",
        "product_measure_amount",
        "product_measure_id",
        "measure_amount",
        "measure_id",
    ];

    protected $casts = [
        "product_id"            => "integer",
        "product_measure_amount"=> "float",
        "product_measure_id"    => "integer",
        "measure_amount"        => "float",
        "measure_id"            => "integer",
        "created_at"            => "datetime:Y-m-d H:m:s",
        "updated_at"            => "datetime:Y-m-d H:m:s"

    ];
}
