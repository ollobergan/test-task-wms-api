<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    use HasFactory;

    protected $table = "storages";

    protected $fillable = [
        "name",
        "address",
    ];

    protected $casts = [
        "name"          => "string",
        "address"       => "string",
        "created_at"    => "datetime:Y-m-d H:m:s",
        "updated_at"    => "datetime:Y-m-d H:m:s"

    ];
}
