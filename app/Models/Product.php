<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = [
        "category_id",
        "name",
        "price",
        "measure_id"
    ];

    protected $casts = [
        "category_id"   => "integer",
        "name"          => "string",
        "price"         => "float",
        "measure_id"    => "integer",
        "created_at"    => "datetime:Y-m-d H:m:s",
        "updated_at"    => "datetime:Y-m-d H:m:s"

    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function measures()
    {
        return $this->hasMany(ProductMeasure::class,'product_id','id');
    }

    public function avialable()
    {
        $relation = $this->hasMany(OrderItem::class,'product_id','id');
        return $relation;
    }
}

