<?php


namespace App\Services;


use App\Models\OrderItem;
use Illuminate\Validation\ValidationException;

class OrderProductService
{
    private $order_id;

    public function __construct($order_id = null)
    {
        $this->order_id = $order_id;
    }

    public function checkProductForBuy($product, $storage_id)
    {
        $measure_data = (new ProductService())->convertToProductMeasure($product['id'],$product['measure_id'],$product['amount']);
        $avialable_product = $this->getAvialableProductBuy($product['id'],$storage_id);

        if ($measure_data['amount']>$avialable_product){
            throw ValidationException::withMessages([
                'amount' => trans('api.product_amount_not_avialable')
            ]);
        }
        return true;
    }

    public function getAvialableProductBuy($product_id,$storage_id)
    {
        $order_product = OrderItem::query()
            ->selectRaw("sum(case when order_type in ('sell','buy_refund') then -1*product_amount else product_amount end) as product_amount");
        if ($this->order_id != null){
            $order_product = $order_product->where('order_id','=',$this->order_id );
        }
        $order_product = $order_product
            ->where('product_id','=',$product_id)
            ->where('storage_id','=',$storage_id)
            ->first();
        return number_format((float)$order_product->product_amount, 2, '.', '');
    }

}
