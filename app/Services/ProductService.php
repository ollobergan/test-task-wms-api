<?php


namespace App\Services;


use App\Models\Product;
use App\Models\ProductMeasure;
use App\Models\ProviderCategory;
use Illuminate\Validation\ValidationException;

class ProductService
{
    /**
     * Checking provider product category
     *
     * @param $product_id
     * @param $provider_id
     * @return bool
     * @throws ValidationException
     */
    public function checkProviderProduct($product_id,$provider_id)
    {

        $product = Product::query()
            ->with('category')
            ->where('id','=',$product_id)
            ->firstOrFail();

        // Checking product category
        $provider_category_cnt = ProviderCategory::query()
            ->where('account_id','=',$provider_id)
            ->where('category_id','=',$product->category->id)
            ->count();
        if ($provider_category_cnt > 0){
            return true;
        }

        // Checking product category parent categories
        $provider_category_cnt = ProviderCategory::query()
            ->where('account_id','=',$provider_id)
            ->where('category_id','=',$product->category->parent->id)
            ->count();
        if ($provider_category_cnt > 0){
            return true;
        }

        // If category not belong to provider
        throw ValidationException::withMessages([
            'product' => trans('api.product_not_belong_to_provider')
        ]);
    }

    /**
     * Method for checking product measures
     *
     * @param $product_id
     * @param $measure_id
     * @return bool
     * @throws ValidationException
     */
    public function checkProductMeasure($product_id,$measure_id)
    {
        $product = Product::query()
            ->with('measures')
            ->where('id','=',$product_id)
            ->first();
        // Checking product main measure
        if ($product->measure_id == $measure_id){ return true; }

        // Checking product additional measures
        foreach ($product->measures as $measure){
            if ($measure->measure_id == $measure_id){
                return true;
            }
        }

        // product measure not identical
        throw ValidationException::withMessages([
            'product' => trans('api.wrong_product_measure')
        ]);
    }

    /**
     * Convert measure to product measure
     *
     * @param $product_id
     * @param $measure_id
     * @param $amount
     * @return array
     */
    public function convertToProductMeasure($product_id, $measure_id, $amount)
    {
        $data = [];
        $product = Product::find($product_id);
        if ($product->measure_id != $measure_id) {
            $productMeasure = ProductMeasure::query()
                ->where('product_id', '=', $product_id)
                ->where('measure_id', '=', $measure_id)
                ->first();
            $data['amount']     = number_format((float)(($amount*$productMeasure->product_measure_amount)/$productMeasure->measure_amount), 2, '.', '');
            $data['measure_id'] = $productMeasure->product_measure_id;
        }else{
            $data['amount']     = number_format((float)$amount, 2, '.', '');
            $data['measure_id'] = $measure_id;
        }
        return $data;
    }
}
