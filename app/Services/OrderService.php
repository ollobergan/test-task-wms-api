<?php


namespace App\Services;


use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ProductMeasure;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * Creating provider buy product order
     *
     * @param $data
     * @return Order
     * @throws \ErrorException
     */
    public function createOrder($data)
    {
        try {
            DB::beginTransaction();

            //Create order
            $order                  = new Order();
            $order->account_id      = $data['account_id'];
            $order->storage_id      = $data['storage_id'];
            $order->more            = $data['more']??"";
            $order->order_date      = Carbon::now();
            $order->order_type      = $data['order_type'];
            $order->order_total_sum = 0;
            $order->user_id         = auth()->user()->id;
            $order->save();

            $total_sum = 0;
            //Create order items
            foreach ($data['products'] as $item){
                // Set default measure id to order item
                $measure_data = (new ProductService())->convertToProductMeasure($item['id'],$item['measure_id'],$item['amount']);

                $order_item                     = new OrderItem();
                $order_item->order_id           = $order->id;
                $order_item->account_id         = $data['account_id'];
                $order_item->storage_id         = $data['storage_id'];
                $order_item->order_type         = $data['order_type'];
                $order_item->product_id         = $item['id'];
                $order_item->product_amount     = $measure_data['amount'];
                $order_item->product_measure_id = $measure_data['measure_id'];
                $order_item->amount             = $item['amount'];
                $order_item->measure_id         = $item['measure_id'];
                $order_item->product_price      = $item['price'];
                $order_item->user_id            = auth()->user()->id;
                $order_item->save();
                $total_sum += $measure_data['amount']*$item['price'];
            }
            $order->order_total_sum = $total_sum;
            $order->save();
            DB::commit();
            return $order;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \ErrorException(trans('api.internal_server_error'));
        }
    }
}
