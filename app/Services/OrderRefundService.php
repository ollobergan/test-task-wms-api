<?php


namespace App\Services;


use App\Models\OrderItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderRefundService
{
    private $order_id;

    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

    public function refundBuyedProduct($data)
    {
        try {
            DB::beginTransaction();
            foreach ($data as $item){
                $orderItemOld = OrderItem::query()
                    ->where('order_id','=',$this->order_id)
                    ->where('product_id','=',$item['id'])
                    ->where('order_type','=','buy')
                    ->first();

                $measure_data = (new ProductService())->convertToProductMeasure($item['id'],$item['measure_id'],$item['amount']);

                $order_item                     = new OrderItem();
                $order_item->order_id           = $this->order_id;
                $order_item->account_id         = $orderItemOld->account_id;
                $order_item->storage_id         = $orderItemOld->storage_id;
                $order_item->order_type         = "buy_refund";
                $order_item->product_id         = $item['id'];
                $order_item->product_amount     = $measure_data['amount'];
                $order_item->product_measure_id = $measure_data['measure_id'];
                $order_item->amount             = $item['amount'];
                $order_item->measure_id         = $item['measure_id'];
                $order_item->product_price      = $orderItemOld->product_price;
                $order_item->more               = $item['more'];
                $order_item->user_id            = auth()->user()->id;
                $order_item->save();
            }
            DB::commit();
            return true;
        }catch (\Exception $exception){
            DB::rollBack();
            throw new \ErrorException(trans('api.internal_server_error'));
        }
    }

}
