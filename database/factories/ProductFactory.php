<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Measure;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "category_id"   => Category::query()->whereNotNull('parent_id')->inRandomOrder()->first()->id,
            "name"          => fake()->name,
            "price"         => rand(100,100000),
            "measure_id"    => Measure::query()->inRandomOrder()->first()->id
        ];
    }
}
