<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Account;
use App\Models\Category;
use App\Models\Measure;
use App\Models\Product;
use App\Models\ProductMeasure;
use App\Models\ProviderCategory;
use App\Models\Storage;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Category::factory(10)->create(['parent_id'=>null])
            ->each(function ($item){
                Category::factory(3)->create(['parent_id'=>$item->id]);
            });

        Product::factory(30)->create()
            ->each(function ($item){
                $measures = Measure::select('id')
                    ->where('id','!=',$item->measure_id)
                    ->inRandomOrder()
                    ->limit(3)
                    ->get();
                foreach ($measures as $measure){
                    ProductMeasure::factory()
                        ->create([
                            'product_id'        => $item->id,
                            'measure_id'        => $measure->id,
                            'product_measure_id'=> $item->measure_id]);
                }
            });
        Storage::factory(5)->create();
        Account::factory(20)->create(['account_type'=>'provider'])
            ->each(function ($item){
                $categories = Category::query()
                    ->inRandomOrder()
                    ->limit(4)
                    ->get();
                foreach ($categories as $category){
                    ProviderCategory::factory()
                        ->create([
                            'account_id'    => $item->id,
                            'category_id'   => $category->id
                        ]);
                }
            });
        Account::factory(20)->create(['account_type'=>'client']);

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'admin@admin.com',
        ]);
    }
}
