<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_measures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->float("product_measure_amount");
            $table->unsignedBigInteger('product_measure_id');
            $table->float("measure_amount");
            $table->unsignedBigInteger('measure_id');
            $table->timestamps();
            $table->foreign('measure_id')->references('id')->on('measures');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('product_measure_id')->references('measure_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_measures');
    }
};
