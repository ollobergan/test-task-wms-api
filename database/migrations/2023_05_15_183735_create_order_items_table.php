<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('account_id');
            $table->unsignedBigInteger('storage_id');
            $table->enum('order_type',['buy','sell','buy_refund','sell_refund']);
            $table->unsignedBigInteger('product_id');
            $table->float("product_amount");
            $table->unsignedBigInteger('product_measure_id');
            $table->float("amount");
            $table->unsignedBigInteger('measure_id');
            $table->decimal("product_price");
            $table->string('more')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('storage_id')->references('id')->on('storages');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order_items');
    }
};
