<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login',[\App\Http\Controllers\AuthController::class,'login'])->name('login');

Route::middleware('auth:sanctum')->group(function (){
    Route::get('/user', function (Request $request) {return $request->user();});

    Route::get('/products/available',       [\App\Http\Controllers\ProductController::class,'getAvailableProducts']);
    Route::post('/order/buy',               [\App\Http\Controllers\OrderController::class,'buy_products']);
    Route::post('/order/sell',               [\App\Http\Controllers\OrderController::class,'sell_products']);
    Route::post('/order/buy-refund',        [\App\Http\Controllers\OrderController::class,'refund_buyed_products']);
});

