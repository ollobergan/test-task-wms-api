<?php

return [

    'success'                           => 'Success',
    'unauthorized'                      => 'Unauthorized',
    'login_invalid'                     => 'Login invalid',
    'authorized'                        => 'Authorized',
    'user_not_found'                    => 'User not found',
    'wrong_parameter'                   => 'Wrong parameter',
    'bad_request'                       => 'Bad request',
    'internal_server_error'             => 'Internal server error',
    'data_not_found'                    => 'Data not found',
    'product_not_belong_to_provider'    => 'This product does not belong to provider.',
    'wrong_product_measure'             => 'Wrong product measure.',
    'product_amount_not_avialable'      => 'Product amount not avialable.',
];
